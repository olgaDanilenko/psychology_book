var gulp = require('gulp'),
    minifyCss = require('gulp-minify-css'),
    livereload = require('gulp-livereload'),
    sass = require('gulp-sass'),
    connect = require('gulp-connect'),
    wiredep = require('wiredep').stream,
    gulpif = require('gulp-if'),
    uglify = require('gulp-uglify');

// server connect
gulp.task('connect', function() {
    connect.server({
        root: 'app',
        livereload: true
    });
});

// css
gulp.task('css', function () {
    gulp.src('app/styles/*.sass')
        .pipe(sass())
        .pipe(connect.reload());
});

//bower
gulp.task('bower', function () {
    gulp.src('./app/index.html')
        .pipe(wiredep({
            directory: "app/bower_components"
        }))
        .pipe(gulp.dest('./app'));
});

//build
gulp.task('html', function () {
    return gulp.src('app/*.html')
        .pipe(gulpif('*.js', uglify()))
        .pipe(gulpif('*.css', minifyCss()))
        .pipe(gulp.dest('dist'))
        .pipe(connect.reload());
});

//watch
gulp.task('watch', function ()
{
    gulp.watch('app/styles/*.css', ['css']);
    gulp.watch('app/*.html', ['html']);
    gulp.watch('bower.json', ['bower']);
});

//default
gulp.task('default', ['connect', 'html', 'css', 'watch']);