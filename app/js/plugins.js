(function($){

    $.fn.snow = function(options){

        var $flake 			= $('<div id="flake" />').css({'position': 'absolute', 'top': '-20px', 'width': '50px',
                                                    'height': '50px'}),
            documentHeight 	= $(document).height(),
            documentWidth	= $(document).width(),
            options			= $.extend({}, options);

        var startPositionLeft, startOpacity, endPositionTop, endPositionLeft,
            durationFall, imageIndex, flakeDuplicate,ignoreElements;
        setInterval( function(){

            for(var i = 0; i < options['count']; i++) {
                startPositionLeft = Math.random() * documentWidth - 100;
                startOpacity = 0.5 + Math.random();
                endPositionTop = documentHeight - 50;
                endPositionLeft = startPositionLeft - 100 + Math.random() * 200;
                durationFall = options['delay'][1] + Math.random() * 4000;
                imageIndex = Math.floor(Math.random() * options['images'].length);
                ignoreElements = options['ignore'].css({'position': 'relative', 'z-index': '10'});
                flakeDuplicate = $flake
                    .clone()
                    .appendTo('body');
                flakeDuplicate
                    .css(
                        {
                            left: startPositionLeft,
                            opacity: startOpacity,
                            background: 'url(' + options['images'][imageIndex] + ')',
                            "background-size": "cover",
                            "z-index": i
                        }
                    )
                    .animate(
                        {
                            top: endPositionTop,
                            left: endPositionLeft
                        },
                        durationFall,
                        'linear',
                        function () {
                            $(this).remove()
                        }
                    );
            }
        }, options['delay'][0]);

    };

})(jQuery);